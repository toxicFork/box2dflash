package TestBed 
{
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Common.Math.b2Mat22;
	import Box2D.Common.Math.b2Math;
	import Box2D.Common.Math.b2Transform;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Dynamics.b2Fixture;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.Joints.b2FrictionJointDef;
	import General.Input;
	/**
	 * ...
	 * @author ...
	 */
	public class TestApplyForce extends Test
	{
		private var controlBody:b2Body;
		public function TestApplyForce() 
		{
			world.SetGravity(new b2Vec2(0, 0));
			const restitution:Number = 0.4;
			
			// Set Text field
			Main.m_aboutText.text = "Apply Force";
			
			var ground:b2Body = world.GetGroundBody();
			{
				var xf1:b2Transform = new b2Transform();
				xf1.R.Set(0.3524 * Math.PI);
				
				var tMat:b2Mat22 = xf1.R;
				var tVec:b2Vec2 = new b2Vec2(1,0);
				xf1.position.x = (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y);
				xf1.position.y = (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y);
				
				var vertices:Array = new Array();
				tVec = new b2Vec2(-1,0);
				vertices[0] = new b2Vec2(
								xf1.position.x + (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y),
								xf1.position.y + (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y));
				tVec = new b2Vec2(1, 0);
				vertices[1] = new b2Vec2(
								xf1.position.x + (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y),
								xf1.position.y + (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y));
				tVec = new b2Vec2(0, 0.5);
				vertices[2] = new b2Vec2(
								xf1.position.x + (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y),
								xf1.position.y + (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y));
				
				var poly1:b2PolygonShape = new b2PolygonShape();
				poly1.SetAsArray(vertices, 3);
				
				var sd1:b2FixtureDef = new b2FixtureDef();
				sd1.shape = poly1;
				sd1.density = 4.0;
				
				var xf2:b2Transform = new b2Transform();
				xf2.R.Set( -0.3524 * Math.PI);
				
				
				tMat = xf2.R;
				tVec = new b2Vec2(-1,0);
				xf2.position.x = (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y);
				xf2.position.y = (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y);
				
				
				tVec = new b2Vec2(-1,0);
				vertices[0] = new b2Vec2(
								xf2.position.x + (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y),
								xf2.position.y + (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y));
				tVec = new b2Vec2(1, 0);
				vertices[1] = new b2Vec2(
								xf2.position.x + (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y),
								xf2.position.y + (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y));
				tVec = new b2Vec2(0, 0.5);
				vertices[2] = new b2Vec2(
								xf2.position.x + (tMat.col1.x * tVec.x + tMat.col2.x * tVec.y),
								xf2.position.y + (tMat.col1.y * tVec.x + tMat.col2.y * tVec.y));
				
				var poly2:b2PolygonShape = new b2PolygonShape();
				poly2.SetAsArray(vertices, 3);
				
				var sd2:b2FixtureDef = new b2FixtureDef();
				sd2.shape = poly2;
				sd2.density = 2.0;
				
				var bd:b2BodyDef = new b2BodyDef();
				bd.type = b2Body.b2_dynamicBody;
				bd.angularDamping = 5.0;
				bd.linearDamping = 0.1;
				
				bd.position.Set(0, 2);
				bd.angle = Math.PI;
				
				controlBody = world.CreateBody(bd);
				controlBody.CreateFixture(sd1);
				controlBody.CreateFixture(sd2);
			}
			{
				var shape:b2PolygonShape = new b2PolygonShape();
				shape.SetAsBox(0.5, 0.5);
				
				var fd:b2FixtureDef = new b2FixtureDef();
				fd.shape = shape;
				fd.density = 1.0;
				fd.friction = 0.3;
				
				for (var i:int = 0; i < 10; i++) {
					bd = new b2BodyDef();
					bd.type = b2Body.b2_dynamicBody;
					
					bd.position.Set(0, 5 + 1.54 * i);
					var body:b2Body = world.CreateBody(bd);
					
					body.CreateFixture(fd);
					
					var gravity:Number = 10;
					var I:Number = body.GetInertia();
					var mass:Number = body.GetMass();
					
					//for a circle: I = 0.5 * m * r * r => r = sqrt(2*I/m)
					var radius:Number = Math.sqrt(2 * I / mass);
					
					var jd:b2FrictionJointDef = new b2FrictionJointDef();
					jd.localAnchorA.SetZero();
					jd.localAnchorB.SetZero();
					jd.bodyA = ground;
					jd.bodyB = body;
					jd.collideConnected = true;
					jd.maxForce = mass * gravity;
					jd.maxTorque = mass * radius * gravity;
					
					world.CreateJoint(jd);
				}
				
			}
		}
		
		override public function Update():void 
		{
			super.Update();
			
			if (Input.isKeyDown(87)) {
				var f:b2Vec2 = controlBody.GetWorldVector(new b2Vec2(0, -200));
				var p:b2Vec2 = controlBody.GetWorldPoint(new b2Vec2(0, 2));
				controlBody.ApplyForce(f, p);
			}
			else if (Input.isKeyDown(65)) {
				controlBody.ApplyTorque(50);
			}
			else if (Input.isKeyDown(68)) {
				controlBody.ApplyTorque(-50);
			}
			//Input.isKeyPressed();
		}
		
	}

}